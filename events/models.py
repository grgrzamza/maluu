from django.db import models

class Place(models.Model):
    id = models.CharField(max_length = 30, primary_key = True)
    name = models.TextField()
    website = models.TextField(blank = True, null = True)
    address = models.TextField()
    latitude = models.FloatField()
    longitude = models.FloatField()
    picture_url = models.TextField(blank = True, null = True)
    
    def __str__(self):
        return 'Place: ' + self.name



class Event(models.Model):
    id = models.CharField(max_length = 30, primary_key = True)
    title = models.CharField(max_length = 100)
    text = models.TextField()
    picture_url = models.TextField()
    date = models.DateField(blank = True, null = True)
    time = models.TimeField(blank = True, null = True)
    place = models.ForeignKey(Place, on_delete = models.SET_NULL, blank = True, null = True)
    price = models.IntegerField()    

    def __str__(self):
        return 'Event: ' + self.title            


