from django.shortcuts import get_object_or_404, render
from django.http import HttpResponseRedirect, HttpResponse
from django.template import loader
from django.urls import reverse

import random, os

from .models import Event

def get_images(path, amount):
    BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    dir = os.path.join(BASE_DIR,path)
    images = []
    for i in range(0, amount):
        images.append(random.choice([os.path.join(path,x) for x in os.listdir(dir)
            if os.path.isfile(os.path.join(dir, x))]))                
    return images

def index(request):
    events = Event.objects.all().order_by('date')
    relative_path = "static/events/photos"    
    images = get_images(relative_path, 6)
    template = loader.get_template('events/index.html')
    context = {
        'events': events,
        'images': images
    }
    return HttpResponse(template.render(context, request))

def events(request, event_id):
    event = Event.objects.get(pk=event_id)    
    template = loader.get_template('events/event.html')
    context = {
        'event': event
    }
    return HttpResponse(template.render(context, request))
    
