from django.contrib import admin
from .models import Event, Place

admin.site.register(Event)
admin.site.register(Place)
